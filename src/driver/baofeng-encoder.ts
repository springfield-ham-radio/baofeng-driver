import { RadioChannel, RadioProgram, RadioProgrammedChannel, RadioTone, RadioToneType, RadioMemory } from '@springfield/ham-radio-api';
import { RadioMemorySegment, RadioSegmentedMemory } from '@springfield/ham-radio-driver-utils';
import { formatChannel, formatSegment } from '@springfield/ham-radio-driver-utils';
import { BaofengConfig } from './baofeng-config.js';
import { valuesByDcs } from './baofeng-dcs-tones.js';
import { LogLayer } from 'loglayer';

export class BaofengEncoder {
  private radioModelId: string;
  private config: BaofengConfig;
  private logger: LogLayer;

  constructor(radioModelId: string, config: BaofengConfig, logger: LogLayer) {
    this.radioModelId = radioModelId;
    this.config = config;
    this.logger = logger;
  }

  public encode(radioProgram: RadioProgram): RadioMemory {
    const memory = new RadioSegmentedMemory();

    //---- Channel memory segments ----------------------------------------------------------------

    for (let address = this.config.channelMemorySegment.startAddress; address < this.config.channelMemorySegment.endAddress; address += this.config.memorySegmentSize) {
      memory.addSegment(this.createSegment(address));
    }

    //---- Settings memory segments ---------------------------------------------------------------

    for (let address = this.config.settingsMemorySegment.startAddress; address < this.config.settingsMemorySegment.endAddress; address += this.config.memorySegmentSize) {
      memory.addSegment(this.createSegment(address));
    }

    //---- Encode Channels ------------------------------------------------------------------------

    let channelOffset = 0;
    console.log(`Channel size: ${this.config.channelSize.toString(16)}`);

    for (let channel of radioProgram.channels.sort((a, b) => a.channelNumber - b.channelNumber)) {
      const channelAddress = this.getChannelAddress(channel.channelNumber);
      const encodedChannel = this.encodeChannel(channel);
      this.logger.debug(`Encoded channel ${formatChannel(channel.channelNumber, encodedChannel)}`);
      const segment = memory.findSegment(channelAddress);

      if (segment == undefined) {
        throw new Error(`Could not locate segment for address: '${channelAddress.toString(16)}'`);
      }

      if (segment.startAddress == channelAddress) {
        channelOffset = 0;
      }

      this.addData(segment.data, encodedChannel, channelOffset);

      const radioChannel: RadioChannel = channel.radioChannel as RadioChannel;

      if (radioChannel.name) {
        const encodedChannelName = this.encodeChannelName(radioChannel.name);
        const nameSegmentAddress = 0x1000 + channel.channelNumber * this.config.channelSize;
        const nameSegment = memory.findSegment(nameSegmentAddress);

        if (nameSegment == undefined) {
          throw new Error(`Could not locate segment for address: '${nameSegmentAddress.toString(16)}'`);
        }

        this.addData(nameSegment.data, encodedChannelName, nameSegmentAddress - nameSegment.startAddress);
      }

      channelOffset += this.config.channelSize;
    }

    this.debugMemory(memory);
    return { radioModelId: this.radioModelId, contents: memory };
  }

  private createSegment(address: number): RadioMemorySegment {
    const data = new Uint8Array(this.config.memorySegmentSize);

    for (let i = 0; i < this.config.memorySegmentSize; i++) {
      data[i] = 0xff;
    }

    const segment: RadioMemorySegment = {
      startAddress: address,
      length: this.config.memorySegmentSize,
      data,
    };

    return segment;
  }

  private getChannelAddress(channelNumber: number): number {
    return channelNumber * this.config.channelSize;
  }

  private encodeChannel(programmedChannel: RadioProgrammedChannel): Uint8Array {
    const data: Uint8Array = new Uint8Array(this.config.channelSize);

    data[this.config.powerOffset] = this.encodePower(programmedChannel.settings?.transmitPower as number);

    if (typeof programmedChannel.channelNumber == 'string') {
      throw new Error('Channel references are not supported yet');
    }

    const channel: RadioChannel = programmedChannel.radioChannel as RadioChannel;

    const encodedReceiveFrequency = this.encodeFrequency(channel.receiveFrequency);
    this.addData(data, encodedReceiveFrequency, this.config.receiveFrequencyOffset);

    const encodedTransmitFrequency = this.encodeFrequency(channel.transmitFrequency);
    this.addData(data, encodedTransmitFrequency, this.config.transmitFrequencyOffset);

    const encodedReceiveTone = this.encodeTone(channel.receiveTone);
    this.addData(data, encodedReceiveTone, this.config.receiveToneOffset);

    const encodedTransmitTone = this.encodeTone(channel.transmitTone);
    this.addData(data, encodedTransmitTone, this.config.transmitToneOffset);
    return data;
  }

  private addData(memory: Uint8Array, data: Uint8Array, offset: number) {
    for (let i = 0; i < data.length; i++) {
      memory[offset + i] = data[i];
    }
  }

  private encodeChannelName(channelName: string): Uint8Array {
    let i = 0;
    const data = new Uint8Array(7);

    for (i = 0; i < channelName.length; i++) {
      data[i] = channelName.charCodeAt(i);
    }

    for (let j = i; j < 7; j++) {
      data[j] = 0xff;
    }

    return data;
  }

  private encodePower(power: number): number {
    return power == 5 ? 0x0 : 0x1;
  }

  private encodeFrequency(frequency: number): Uint8Array {
    const data = new Uint8Array(4);

    const eightDigitFrequency = frequency / 10;
    let bcd = parseInt(eightDigitFrequency.toString(10), 16);

    for (let i = 0; i < 4; i++) {
      data[i] = bcd & 0xff;
      bcd >>= 8;
    }

    return data;
  }

  private encodeTone(tone: RadioTone): Uint8Array {
    let value = 0;

    if (tone.type == RadioToneType.DCS) {
      const encodedValue = valuesByDcs.get(tone.tone);

      if (encodedValue == undefined) {
        throw new Error(`Could not encode DCS tone '${tone.tone}'`);
      }

      value = encodedValue;
    } else {
      value = tone.tone * 10;
    }

    const data = new Uint8Array(2);
    data[0] = value >> 8;
    data[1] = value & 0xff;

    return data;
  }

  private debugMemory(memory: RadioSegmentedMemory): void {
    for (const segment of memory.getSegments()) {
      this.logger.debug(formatSegment(segment));
    }
  }
}
