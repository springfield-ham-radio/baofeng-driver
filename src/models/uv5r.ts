import { RadioDriver, RadioModel, RadioSchema } from '@springfield/ham-radio-api';
import { BaofengConfig } from '../driver/baofeng-config.js';
import { BaofengDriver } from '../driver/baofeng-driver.js';
import { BaofengRadio } from '../driver/baofeng-radio.js';
import { LogLayer } from 'loglayer';

export default class UV5R implements BaofengRadio {
  static ID = '9bf0e643-ac61-414f-8c89-a8461f7dbd44';
  static NAME = 'UV-5RE Plus';
  private moduleId: string;
  private logger: LogLayer;
  private model?: RadioModel;
  private schema?: RadioSchema;
  private driver?: RadioDriver;
  private config: BaofengConfig = {
    channelMemorySegment: { startAddress: 0x0, endAddress: 0x1800 },
    settingsMemorySegment: { startAddress: 0x1ec0, endAddress: 0x2000 },
    memorySegmentSize: 0x40,
    magicNumber: [0x50, 0xbb, 0xff, 0x20, 0x12, 0x07, 0x25],
    receiveFrequencyOffset: 0x0,
    transmitFrequencyOffset: 0x4,
    receiveToneOffset: 0x08,
    transmitToneOffset: 0x0a,
    powerOffset: 0x0e,
    channelSize: 0x10,
    numberChannels: 17,
    radioSettingsSchemaPath: '../../assets/uv5r-radio-settings.json',
    channelSettingsSchemaPath: '../../assets/uv5r-channel-settings.json',
  };

  constructor(moduleId: string, logger: LogLayer) {
    this.moduleId = moduleId;
    this.logger = logger;
  }

  async getSchema(): Promise<RadioSchema> {
    if (!this.schema) {
      this.schema = {
        modelId: UV5R.ID,
        settingsSchema: (await import(this.config.radioSettingsSchemaPath)).default,
        channelSchema: (await import(this.config.channelSettingsSchemaPath)).default,
      };
    }

    return this.schema;
  }

  getModel(): RadioModel {
    if (!this.model) {
      this.model = {
        id: UV5R.ID,
        moduleId: this.moduleId,
        name: UV5R.NAME,
        manufacturer: 'Baofeng',
      };
    }

    return this.model;
  }

  getDriver(): RadioDriver {
    if (!this.driver) {
      this.driver = new BaofengDriver(UV5R.ID, this.logger, this.config);
    }

    return this.driver;
  }
}
