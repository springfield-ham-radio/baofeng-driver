import { RadioDriver, RadioModule, RadioModel, RadioSchema } from '@springfield/ham-radio-api';
import UV5R from './models/uv5r.js';
import { BaofengRadio } from './driver/baofeng-radio.js';
import { strict as assert } from 'assert';
import { LogLayer } from 'loglayer';

export default class BaofengModule implements RadioModule {
  private moduleId = 'cc17dc24-ae95-4b02-af02-eb2edca694cd';
  private radiosByModelId = new Map<string, BaofengRadio>();

  constructor(logger: LogLayer) {
    this.radiosByModelId.set(UV5R.ID, new UV5R(this.moduleId, logger));
  }

  public getId(): string {
    return this.moduleId;
  }

  public getModels(): RadioModel[] {
    const models: RadioModel[] = [];

    for (const radio of this.radiosByModelId.values()) {
      models.push(radio.getModel());
    }

    return models;
  }

  public getSchema(modelId: string): Promise<RadioSchema> {
    return this.getRadio(modelId).getSchema();
  }

  public getDriver(modelId: string): RadioDriver {
    return this.getRadio(modelId).getDriver();
  }

  private getRadio(modelName: string): BaofengRadio {
    const model = this.radiosByModelId.get(modelName);
    assert(model);
    return model;
  }
}
